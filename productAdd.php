<?php include('connect.php') ?>

<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="styles.css">
        <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
                crossorigin="anonymous"></script>
        <script src="validate2.js"></script>

    </head>

    <body>

        <div id="header">
            <div id="add">
                <h1>Product Add</h1>
            </div>
            <div align="right"id="butt" >
                <input type="submit" form="forms" class="shadow-lg" name="save"id="save"value="Save" >
                <input type="button" class="shadow-lg" name="back" id="back" value="Back to List" >
                <script type="text/javascript">
                    document.getElementById("back").onclick = function () {
                        location.href = "http://localhost/scandiweb/Junior/productList.php";
                    };
                </script>
            </div>

        </div>
        <hr>
        <div id="main">
            <div id="prod">
                <form id="forms" action="database.php" name="myForm" method="post" onsubmit="validateForm()" >
                    <label for="sku" class="product">SKU</label>
                    <input type="text" name="sku"id="sku" maxLength="10" >
                    <span class="showError" id="sku_error"></span>
                    <br><br>
                    <label for="name" class="product">Name</label>
                    <input type="text" name="name" id="name" maxlength="50"   ><br><br>
                    <label for="price" class="product">Price</label>
                    <input type="text" name="price" id="price" >
                    <br><br>
                    Type Switcher <select name="formType" id="switcher" >
                    <option value="TypeSwitcher" hidden></option>
                    <option value="disc" name="disc" id="disc" >DVD-disc</option>
                    <option value="book" id="book" >Book</option>
                    <option value="furniture" id="furniture" >Furniture</option>
                    </select>

                    <script type="text/javascript">
                        /*$(function() {
                        $('#size').hide();
                        $('#dimensions').hide();
                        $('#weight').hide();
                        $('#switcher').change(function(){
                            showForm($(this).val());
                        });

                    });

                    function showForm(myFormType){
                        if(myFormType == 'disc'){
                            $('#size').show();
                            $('#dimensions').hide();
                            $('#weight').hide();
                        }
                        else if(myFormType == 'book'){
                            $('#weight').show();
                            $('#dimensions').hide();
                            $('#size').hide();

                        }
                        else if(myFormType == 'furniture'){
                            $('#dimensions').show();
                            $('#weight').hide();
                            $('#size').hide();
                        }
                    }*/
                        /*$(function() {
                        $('#switcher').change(function(){
                            showForm($(this).val());
                        });
                    });
                    function showForm(myFormType){*/
                        $('#switcher').on('change',function(){
                            /*alert($('#switcher').val());*/

                            /*var myFormType = $("#switcher option:selected",$(this))
                        .val();*/
                            var myFormType = $(this).val();


                            //switch type of product
                            if(myFormType == 'furniture'){
                                $('#addLabel').html('Height');
                                $('#addLabel1').html('<input type="text" name="height"id="height"><br><br>');

                                $('#addLabel2').html('<label for="width" class="product" id="widthLab" >Width</label>'
                                                    );
                                $('#addLabel3').html('<input type="text" name="width"id="width"><br><br>'
                                                    );
                                $('#addLabel4').html('<label for="length" class="product" id="lengthLab">Length</label>');
                                $('#addLabel5').html('<input type="text" name="length"id="length">');
                            }
                            else if(myFormType == 'disc'){
                                $('#width').remove();
                                $('#length').remove();
                                $('#widthLab').remove();
                                $('#lengthLab').remove();

                                $('#addLabel').text('Size(in mb)');
                                $('#addLabel1').html('<input type="text"  name="size"id="size">');



                            }
                            else if(myFormType == 'book'){
                                $('#width').remove();
                                $('#length').remove();
                                $('#widthLab').remove();
                                $('#lengthLab').remove();


                                $('#addLabel').text('Weight (in KG)');
                                $('#addLabel1').html('<input type="text"  name="weight"id="weight">');

                            }

                        });
                    </script>

                    <br><br>
                    <label id="addLabel" class="product"></label>
                    <label id="addLabel1"></label>
                    <label id="addLabel2" name="addLabel2" class="product"></label>
                    <label id="addLabel3"></label>
                    <label id="addLabel4" class="product"></label>
                    <label id="addLabel5"></label>
                    <br>
                    <br>
                </form>
            </div>

            <div id="switch">

                <!--<p id="message" name="message"></p>-->




            </div>


            <div id="form">



                <!--<form id="size" >
<label for="size" class="product">Size</label>
<input type="text"  name="size"id="size">
<br><br>
</form>-->
                <!--<form id="dimensions">
<label for="Height" class="product">Height</label>
<input type="text" name="height"id="height">
<br><br>
<label for="width" class="product">Width</label>
<input type="text" name="width"id="width">
<br><br>
<label for="length" class="product">Length</label>
<input type="text" name="length"id="length">
<br><br>
</form>
<form id="weight"><label for="weight" class="product">Weight</label>
<input type="text" name="weight"id="weight"></form>-->
            </div>
        </div>

    </body>
</html>
