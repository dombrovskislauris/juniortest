function validateForm() {
    var x = document.forms["myForm"]["sku"].value;
    var y = document.forms["myForm"]["name"].value;
    var t = document.forms["myForm"]["price"].value;
    if(x == "") {
        alert("SKU must be filled out");
        return false;
    }
    if (y == "") {
        alert("Name must be filled out");
        return false;
    }
    if (t == "") {
        alert("Price must be filled out");
        return false;
    }
}

var error_sku = false;
var error_name = false;
$("#sku").focusout(function(){
    check_sku();
});
function check_sku(){

    var pattern = /^[a-zA-Z]*$/;
    var sku = $("#sku").val();
    var name =$("#name").val();
    if (pattern.test(sku) && sku !== ''){
        $("#sku_error").hide();
        $("#sku").css("border-bottom","2px solid #34F458");
    }
    else{
        $("#sku_error").html("Should contain only Characters");
        $("#sku_error").show();
        $("#sku").css("border-bottom","2px solid #F90A0A");
        error_sku = true;
    }
    if (pattern.test(name) && name !== ''){
        $("#name_error").hide();
        $("#name").css("border-bottom","2px solid #34F458");
    }
    else{
        $("#name_error").html("Should contain only Characters");
        $("#name_error").show();
        $("#name").css("border-bottom","2px solid #F90A0A");
        error_name = true;
    }


}
